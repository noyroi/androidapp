package tenpass.myapplication1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class NewGameActivity extends AppCompatActivity {

    private ArrayList<String> dates = new ArrayList<String>();
    private HashMap<String, ArrayList<String>> datesAndTimes = new HashMap<String, ArrayList<String>>();
    private ArrayList<String> currentTimes;
    private HashMap<String, HashMap<String, Integer>> busyDatesAndTimes = new HashMap<String, HashMap<String, Integer>>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_game);
        int courtId = (int) getIntent().getExtras().get("courtId");
        String courtCity = (String) getIntent().getExtras().get("courtCity");
        String courtName = (String) getIntent().getExtras().get("courtName");
        ((TextView) findViewById(R.id.toolbarTitle)).setText("משחק חדש ב" + courtName);
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        ((TextView) findViewById(R.id.toolbarTitle)).setTypeface(font);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        NewGameActivity.ConnectSocket mAuthTask = new NewGameActivity.ConnectSocket(String.valueOf(courtId), this);
        mAuthTask.execute();

    }

    public void submitNewGame(View view) {
        Spinner spinnerDate = (Spinner) findViewById(R.id.spinnerDate);
        Spinner spinnerTime = (Spinner) findViewById(R.id.spinnerTime);
        Spinner spinnerDuration = (Spinner) findViewById(R.id.spinnerDuration);
        EditText numOfPlayers = (EditText) findViewById(R.id.numOfPlayers);
        CheckBox privateOrNot = (CheckBox) findViewById(R.id.privateOrNot);

        if (spinnerDuration.getSelectedItem()==null || spinnerDuration.getSelectedItem().toString().equals("משך משחק")||
                numOfPlayers.getText().toString().equals("")) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewGameActivity.this);
            dlgAlert.setMessage("יש למלא את כל השדות");
            dlgAlert.setTitle("אופס...");
            dlgAlert.setCancelable(true);
            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dlgAlert.create().show();
        } else {
            int courtId = (int) getIntent().getExtras().get("courtId");
            NewGameActivity.SubmitGame mAuthTask = new NewGameActivity.SubmitGame(String.valueOf(courtId),
                    spinnerDate.getSelectedItem().toString(),
                    spinnerTime.getSelectedItem().toString(),
                    spinnerDuration.getSelectedItem().toString(),
                    numOfPlayers.getText().toString(), privateOrNot.isChecked(), this);
            mAuthTask.execute();
        }
    }


    public void fillSpinners() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat df2 = new SimpleDateFormat("kk:mm");
        String date = df.format(c.getTime());

        c.add(Calendar.HOUR_OF_DAY, 1);
        c.set(Calendar.MINUTE, 0);
        String time = df2.format(c.getTime());
        ArrayList<String> times = new ArrayList<String>();
        while (!time.equals("23:00") && !time.equals("24:00")) {
            if (busyDatesAndTimes.containsKey(date)) {
                HashMap<String, Integer> busyTimes = busyDatesAndTimes.get(date);
                if (busyTimes.containsKey(time)) {
                    int busyDuration = busyTimes.get(time);
                    c.add(Calendar.MINUTE, busyDuration);
                } else {
                    times.add(time);
                    c.add(Calendar.MINUTE, 30);
                }
            } else {
                times.add(time);
                c.add(Calendar.MINUTE, 30);
            }
            time = df2.format(c.getTime());
        }
        if (!times.isEmpty()) {
            times.add("בחר שעה");
            dates.add(date);
            datesAndTimes.put(date, times);
        }
        c.add(Calendar.DATE, 1);

        for (int i = 1; i < 30; i++) {
            times = new ArrayList<String>();
            date = df.format(c.getTime());
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            for (int j = 0; j < 46; j++) {
                time = df2.format(c.getTime());
                if (busyDatesAndTimes.containsKey(date)) {
                    HashMap<String, Integer> busyTimes = busyDatesAndTimes.get(date);
                    if (busyTimes.containsKey(time)) {
                        int busyDuration = busyTimes.get(time);
                        c.add(Calendar.MINUTE, busyDuration);
                        j += (busyDuration / 30) - 1;
                    } else {
                        times.add(time);
                        c.add(Calendar.MINUTE, 30);
                    }
                } else {
                    times.add(time);
                    c.add(Calendar.MINUTE, 30);
                }
            }
            if (!times.isEmpty()) {
                times.add("בחר שעה");
                dates.add(date);
                datesAndTimes.put(date, times);
            }
            c.add(Calendar.DATE, 1);
        }
        Spinner spinner1 = (Spinner) findViewById(R.id.spinnerDate);

        dates.add("בחר תאריך");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, dates) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(arrayAdapter);
        Drawable spinnerDrawable = spinner1.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner1.setBackground(spinnerDrawable);
        } else {
            spinner1.setBackgroundDrawable(spinnerDrawable);
        }

        spinner1.setSelection(arrayAdapter.getCount());
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) parentView.getChildAt(0)).setTypeface(font);
                fillTimeSpinner(parentView);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void fillTimeSpinner(AdapterView<?> parentView) {
        ArrayList<String> times = datesAndTimes.get(parentView.getSelectedItem().toString());
        if (times != null) {
            currentTimes = times;
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, times) {
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v).setTypeface(font);
                    return v;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    View v = super.getView(position, convertView, parent);
                    if (position == getCount()) {
                        ((TextView) v.findViewById(android.R.id.text1)).setText("");
                        ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                        ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                        ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                        ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                    }
                    return v;
                }

                @Override
                public int getCount() {
                    return super.getCount() - 1;
                }
            };
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Spinner spinner2 = (Spinner) findViewById(R.id.spinnerTime);
            spinner2.setAdapter(arrayAdapter);
            Drawable spinnerDrawable = spinner2.getBackground().getConstantState().newDrawable();
            spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                spinner2.setBackground(spinnerDrawable);
            } else {
                spinner2.setBackgroundDrawable(spinnerDrawable);
            }
            spinner2.setSelection(arrayAdapter.getCount());
            spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) parentView.getChildAt(0)).setTypeface(font);
                    fillDurationSpinner(parentView);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }
            });
        }
    }

    private void fillDurationSpinner(AdapterView<?> parentView) {
        ArrayList<String> durations = new ArrayList<String>();
        durations.add("30 דקות");
        String selectedTime = parentView.getSelectedItem().toString();
        SimpleDateFormat df2 = new SimpleDateFormat("kk:mm");
        try {
            Calendar c = Calendar.getInstance();
            c.setTime(df2.parse(selectedTime));
            c.add(Calendar.MINUTE, 30);
            if (currentTimes.contains(df2.format(c.getTime()))) {
                durations.add("60 דקות");
                c.add(Calendar.MINUTE, 30);
                if (currentTimes.contains(df2.format(c.getTime()))) {
                    durations.add("90 דקות");
                }
            }
            durations.add("משך משחק");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, durations) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) v).setTypeface(font);
                ((TextView) v).setGravity(Gravity.RIGHT);
                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                    ((TextView) v.findViewById(android.R.id.text1)).setGravity(Gravity.RIGHT);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner3 = (Spinner) findViewById(R.id.spinnerDuration);
        spinner3.setAdapter(arrayAdapter);
        Drawable spinnerDrawable = spinner3.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner3.setBackground(spinnerDrawable);
        } else {
            spinner3.setBackgroundDrawable(spinnerDrawable);
        }
        spinner3.setSelection(arrayAdapter.getCount());
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_VIEW_END);
                ((TextView) parentView.getChildAt(0)).setGravity(Gravity.RIGHT);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) parentView.getChildAt(0)).setTypeface(font);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });

    }

    public class ConnectSocket extends AsyncTask<Void, Void, String> {
        private String uid1;
        private Context mContext;

        public ConnectSocket(String u, Context context) {
            this.uid1 = u;
            this.mContext = context;

        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();
            m.put("uid", uid1);
            try {
                s.sendRequest("GC1", m);
                ServerAnswer a = s.getAnswer();

                if (a.getType().equals("GC1") && a.getStatus().equals("success")) {
                    return a.getContent().toString();
                }
            } catch (Exception e) {
                return "";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()) {
                JSONArray answers;
                JSONParser parser = new JSONParser();
                try {
                    answers = (org.json.simple.JSONArray) parser.parse(answer);
                    for (Object obj : answers) {
                        JSONObject gameObject = (JSONObject) obj;
                        TextView txt = new TextView(this.mContext);
                        long duration = (long) (gameObject.get("duration"));
                        String time = (String) (gameObject.get("time"));
                        String date = (String) (gameObject.get("date"));
                        if (!busyDatesAndTimes.containsKey(date)) {
                            HashMap<String, Integer> map = new HashMap<String, Integer>();
                            map.put(time, (int) duration);
                            busyDatesAndTimes.put(date, map);
                        } else {
                            busyDatesAndTimes.get(date).put(time, (int) duration);
                        }
                    }
                    fillSpinners();
                } catch (Exception e) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewGameActivity.this);
                    dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setCancelable(true);
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(NewGameActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    dlgAlert.create().show();

                }
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewGameActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(NewGameActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                dlgAlert.create().show();

            }
        }
    }


    public class SubmitGame extends AsyncTask<Void, Void, String> {
        private String courtId;
        private String date;
        private String time;
        private String duration;
        private String num;
        private boolean priv;
        private Context mContext;

        public SubmitGame(String u, String d, String t, String du, String n, boolean p, Context context) {
            this.courtId = u;
            this.date = d;
            this.time = t;
            this.duration = du;
            this.num = n;
            this.priv = p;
            this.mContext = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();
            m.put("courtId", courtId);
            m.put("creatorId", settings.getString("uid", "0"));
            m.put("date", date);
            m.put("time", time);
            m.put("playerNumber", num);
            if (priv)
                m.put("private", "true");
            else
                m.put("private", "false");
            m.put("duration", (duration.split(" ")[0]));

            try {
                s.sendRequest("GU1", m);
                ServerAnswer a = s.getAnswer();
                if (a.getType().equals("GU1") && a.getStatus().equals("success")) {
                    return a.getContent().toString();
                }
            } catch (Exception e) {
                return "fail";
            }
            return "fail";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.equals("fail")) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewGameActivity.this);
                dlgAlert.setMessage("המשחק נוצר בהצלחה!");
                dlgAlert.setTitle("יופי");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(NewGameActivity.this, GameActivity.class);
                        i.putExtra("id", Integer.parseInt(answer));
                        i.putExtra("fromProfile", true);
                        startActivity(i);
                        setResult(RESULT_OK,null);
                        finish();
                    }
                });
                dlgAlert.create().show();
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(NewGameActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Intent i = new Intent(NewGameActivity.this, MenuActivity.class);
                        // startActivity(i);
                        // finish();
                    }
                });
                dlgAlert.create().show();

            }
        }
    }


}