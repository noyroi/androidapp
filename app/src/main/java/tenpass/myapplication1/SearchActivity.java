package tenpass.myapplication1;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class SearchActivity extends AppCompatActivity {

    private ArrayList<String> CityList = new ArrayList<String>();
    private ArrayList<String> CourtList = new ArrayList<String>();
    private HashMap<String, HashMap<Integer, String>> cityAndCourt;
    private String answer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        TextView t = (TextView)findViewById(R.id.toolbarTitle);
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        t.setTypeface(font);
        fillDatesSpinner();
        fillTimeSpinner();
        fillDurationSpinner();
        SearchActivity.FillCourtSpinner task = new SearchActivity.FillCourtSpinner(this);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void SearchGame(View view) {
        String city = ((Spinner)findViewById(R.id.cities)).getSelectedItem().toString();
        String court = "";
        if(!city.equals("עיר"))
            court = ((Spinner)findViewById(R.id.courts)).getSelectedItem().toString();
        String date = ((Spinner)findViewById(R.id.spinnerDate)).getSelectedItem().toString();
        String time = ((Spinner)findViewById(R.id.spinnerTime)).getSelectedItem().toString();
        String duration = ((Spinner)findViewById(R.id.spinnerDuration)).getSelectedItem().toString();

        SearchActivity.SearchForGames games = new SearchActivity.SearchForGames(this,
                city,court,date,time,duration);
        games.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public void fillDatesSpinner() {
        Calendar c = Calendar.getInstance();
        ArrayList<String> dates = new ArrayList<String>();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String date = df.format(c.getTime());

        for (int i = 1; i < 30; i++) {
            date = df.format(c.getTime());
            dates.add(date);
            c.add(Calendar.DATE, 1);
        }
        dates.add("תאריך");
        Spinner spinner1 = (Spinner) findViewById(R.id.spinnerDate);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, dates) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(arrayAdapter);
        Drawable spinnerDrawable = spinner1.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner1.setBackground(spinnerDrawable);
        } else {
            spinner1.setBackgroundDrawable(spinnerDrawable);
        }

        spinner1.setSelection(arrayAdapter.getCount());
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) parentView.getChildAt(0)).setTypeface(font);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    public void fillTimeSpinner() {
        ArrayList<String> times = new ArrayList<String>();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("kk:mm");
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        String time = df.format(c.getTime());
        for (int i = 1; i < 46; i++) {
            time = df.format(c.getTime());
            times.add(time);
            c.add(Calendar.MINUTE, 30);
        }
        times.add("החל משעה");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, times) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinnerTime);
        spinner2.setAdapter(arrayAdapter);
        Drawable spinnerDrawable = spinner2.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner2.setBackground(spinnerDrawable);
        } else {
            spinner2.setBackgroundDrawable(spinnerDrawable);
        }
        spinner2.setSelection(arrayAdapter.getCount());
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) parentView.getChildAt(0)).setTypeface(font);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }

    private void fillDurationSpinner() {
        ArrayList<String> durations = new ArrayList<String>();
        durations.add("30 דקות");
        durations.add("60 דקות");
        durations.add("90 דקות");
        durations.add("משך משחק");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, durations) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                ((TextView) v).setGravity(Gravity.CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) v).setTypeface(font);
                return v;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                if (position == getCount()) {
                    ((TextView) v.findViewById(android.R.id.text1)).setText("");
                    ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                    ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    ((TextView) v.findViewById(android.R.id.text1)).setGravity(Gravity.CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                    ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                }
                return v;
            }

            @Override
            public int getCount() {
                return super.getCount() - 1;
            }
        };
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner spinner3 = (Spinner) findViewById(R.id.spinnerDuration);
        spinner3.setAdapter(arrayAdapter);
        Drawable spinnerDrawable = spinner3.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner3.setBackground(spinnerDrawable);
        } else {
            spinner3.setBackgroundDrawable(spinnerDrawable);
        }
        spinner3.setSelection(arrayAdapter.getCount());
        spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                ((TextView) parentView.getChildAt(0)).setGravity(Gravity.CENTER);
                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                ((TextView) parentView.getChildAt(0)).setTypeface(font);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
    }
    public class SearchForGames extends AsyncTask<Void, Void, Void> {
        private Context mContext;
        private String city;
        private String court;
        private String date;
        private String time;
        private String duration;
        private boolean flag = true;


        public SearchForGames(SearchActivity searchActivity,
                              String city, String court, String date, String time, String duration) {
            this.mContext = searchActivity;
            this.city = city;
            this.court = court;
            this.date = date;
            this.time = time;
            this.duration = duration;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();
            if (!this.city.equals("עיר")) {
                m.put("city", city);
            }
            if (!this.court.equals("") && !this.court.equals("מגרש")) {
                m.put("court", court);
            }
            if (!this.date.equals("תאריך")) {
                m.put("date", date);
            }
            if (!this.time.equals("החל משעה")) {
                m.put("time", time);
            }
            if (!this.duration.equals("משך משחק")) {
                m.put("duration", duration);
            }

            try {
                s.sendRequest("SG1", m);
                ServerAnswer a = s.getAnswer();
                if (a.getType().equals("SG1") && a.getStatus().equals("success")) {
                    answer = a.getContent();
                    flag = true;
                }
            } catch (Exception e) {
                flag = false;
            }
            return null;
        }

        protected void onPostExecute(final Void x) {
            if (flag) {
                Intent i = new Intent(SearchActivity.this, FoundGamesActivity.class);
                i.putExtra("games", answer);
                startActivity(i);
            }
        }
    }

    public class FillCourtSpinner extends AsyncTask<Void, Void, String> {
        private Context mContext;

        public FillCourtSpinner(SearchActivity searchActivity) {
            this.mContext = searchActivity;
        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();

            try {
                s.sendRequest("CA1", m);
                ServerAnswer a = s.getAnswer();
                if (a.getType().equals("CA1") && a.getStatus().equals("success")) {
                    return a.getContent().toString();
                }
            } catch (Exception e) {
                return "";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()) {
                cityAndCourt = new HashMap<String, HashMap<Integer, String>>();
                JSONArray answers;
                JSONParser parser = new JSONParser();
                try {
                    answers = (org.json.simple.JSONArray) parser.parse(answer);
                    for (Object obj : answers) {
                        JSONObject courtObject = (JSONObject) obj;
                        String city = (String) courtObject.get("city");
                        String name = (String) courtObject.get("name");
                        if (!cityAndCourt.containsKey(city)) {
                            HashMap<Integer, String> cityDic = new HashMap<Integer, String>();
                            cityDic.put((int) ((long) courtObject.get("id")), name);
                            cityAndCourt.put(city, cityDic);
                            CityList.add(city);
                        } else {
                            cityAndCourt.get(city).put((int) ((long) courtObject.get("id")), name);
                        }
                    }
                    CityList.add("עיר");
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this.mContext, android.R.layout.simple_spinner_dropdown_item, CityList) {
                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            View v = super.getDropDownView(position, convertView, parent);
                            ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            ((TextView) v).setGravity(Gravity.CENTER);
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            ((TextView) v).setTypeface(font);
                            return v;
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {

                            View v = super.getView(position, convertView, parent);
                            if (position == getCount()) {
                                ((TextView) v.findViewById(android.R.id.text1)).setText("");
                                ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                                ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                                ((TextView) v.findViewById(android.R.id.text1)).setGravity(Gravity.CENTER);
                                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                                ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                            }
                            return v;
                        }

                        @Override
                        public int getCount() {
                            return super.getCount() - 1;
                        }
                    };
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner citySpinner = (Spinner) findViewById(R.id.cities);
                    citySpinner.setAdapter(arrayAdapter);
                    Drawable spinnerDrawable = citySpinner.getBackground().getConstantState().newDrawable();
                    spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        citySpinner.setBackground(spinnerDrawable);
                    } else {
                        citySpinner.setBackgroundDrawable(spinnerDrawable);
                    }
                    citySpinner.setSelection(arrayAdapter.getCount());
                    citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                            ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            ((TextView) parentView.getChildAt(0)).setGravity(Gravity.CENTER);
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            ((TextView) parentView.getChildAt(0)).setTypeface(font);
                            fillCourtSpinner(parentView);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }
                    });

                } catch (Exception e) {
                    android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(SearchActivity.this);
                    dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setPositiveButton("סבבה", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(SearchActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                }
            } else {
                android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(SearchActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(SearchActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
            }
        }
    }

    private void fillCourtSpinner(AdapterView<?> parentView) {
        HashMap<Integer, String> courts = cityAndCourt.get(parentView.getSelectedItem().toString());
        if (courts != null) {
            CourtList.clear();
            for (String value : courts.values()) {
                CourtList.add(value);
            }
            CourtList.add("מגרש");
            ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CourtList) {
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = super.getDropDownView(position, convertView, parent);
                    ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    ((TextView) v).setGravity(Gravity.CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) v).setTypeface(font);
                    return v;
                }

                @Override
                public View getView(int position, View convertView, ViewGroup parent) {

                    View v = super.getView(position, convertView, parent);
                    if (position == getCount()) {
                        ((TextView) v.findViewById(android.R.id.text1)).setText("");
                        ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                        ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        ((TextView) v.findViewById(android.R.id.text1)).setGravity(Gravity.CENTER);
                        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                        ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                        ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));

                    }
                    return v;
                }

                @Override
                public int getCount() {
                    return super.getCount() - 1;
                }
            };
            arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Spinner courtSpinner = (Spinner) findViewById(R.id.courts);
            courtSpinner.setAdapter(arrayAdapter2);
            Drawable spinnerDrawable = courtSpinner.getBackground().getConstantState().newDrawable();
            spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                courtSpinner.setBackground(spinnerDrawable);
            } else {
                courtSpinner.setBackgroundDrawable(spinnerDrawable);
            }
            courtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    ((TextView) parent.getChildAt(0)).setGravity(Gravity.CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) parent.getChildAt(0)).setTypeface(font);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            courtSpinner.setSelection(arrayAdapter2.getCount());
        }
    }
}
