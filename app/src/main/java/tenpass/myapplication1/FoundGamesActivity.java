package tenpass.myapplication1;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FoundGamesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_found_games);

        TextView t = (TextView)findViewById(R.id.gamestitle);
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        t.setTypeface(font);

        String answer = (String) getIntent().getExtras().get("games");
        JSONParser parser = new JSONParser();
        try {
            JSONArray games = (JSONArray) parser.parse(answer);
            LinearLayout linearLayout = (LinearLayout)findViewById(R.id.games);
            LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lparams.gravity = Gravity.RIGHT;
            lparams.setMargins(10, 20, 25, 10);
            if (games.size() < 1){
                lparams.gravity = Gravity.CENTER;
                TextView txt = new TextView(this);
                txt.setLayoutParams(lparams);
                String cont = "לא נמצאו תוצאות התואמות את החיפוש";
                txt.setText(cont);
                txt.setTypeface(font);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                txt.setTextColor(Color.parseColor("#000000"));
                linearLayout.addView(txt);
            }
            int i = 0;
            Calendar c = Calendar.getInstance();
            Date today = c.getTime();
            for (Object obj: games) {
                JSONObject gameObject = (JSONObject) obj;
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                c.setTime(df.parse((String) (gameObject.get("date"))));
                if (today.after(c.getTime())){
                    continue;
                }
                LinearLayout lh = new LinearLayout(this);
                lh.setOrientation(LinearLayout.HORIZONTAL);
                lh.setGravity(Gravity.RIGHT);
                ImageView img = new ImageView(this);
                switch (i%9){
                    case 0:
                        img.setImageResource(R.drawable.icon1);
                        break;
                    case 1:
                        img.setImageResource(R.drawable.icon2);
                        break;
                    case 2:
                        img.setImageResource(R.drawable.icon3);
                        break;
                    case 3:
                        img.setImageResource(R.drawable.icon4);
                        break;
                    case 4:
                        img.setImageResource(R.drawable.icon5);
                        break;
                    case 5:
                        img.setImageResource(R.drawable.icon6);
                        break;
                    case 6:
                        img.setImageResource(R.drawable.icon7);
                        break;
                    case 7:
                        img.setImageResource(R.drawable.icon8);
                        break;
                    case 8:
                        img.setImageResource(R.drawable.icon9);
                        break;
                }
                TextView txt = new TextView(this);
                txt.setLayoutParams(lparams);
                txt.setTypeface(font);
                String cont = "תאריך:" + " " + (String) (gameObject.get("date")) + "\n" +
                        "שעה:" + " " + (String) (gameObject.get("time")) + "\n" +
                        "משך:" + " " + String.valueOf((long)(gameObject.get("duration")))+" " +"דקות"+ "\n" +
                        "מארגן:" + " " + (String) (gameObject.get("creator"));
                txt.setText(cont);
                txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                txt.setTextColor(Color.parseColor("#000000"));
                txt.setId((int)((long)gameObject.get("id")));
                txt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int uid = ((TextView)v).getId();
                        Intent i = new Intent(FoundGamesActivity.this, GameActivity.class);
                        i.putExtra("id", uid);
                        i.putExtra("fromProfile", false);
                        startActivity(i);
                    }
                });
                //linearLayout.addView(txt);
                lh.addView(txt);
                //ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(130,130);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(130, 130);
                lp.setMargins(10, 100, 10, 10);
                img.setLayoutParams(lp);
                //img.setLayoutParams(params);
                lh.addView(img);
                linearLayout.addView(lh);

                View line = new View(this);
                line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1));
                line.setBackgroundColor(Color.GRAY);

                linearLayout.addView(line);
                i++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
