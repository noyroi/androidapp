package tenpass.myapplication1;
import android.hardware.SensorEventListener;
import android.os.AsyncTask;

import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
/**
 * Created by Noy on 05/04/2017.
 */
public class ServerConnector {
    private Socket sock;
    private DataOutputStream outToServer;
   // private BufferedReader inFromServer;
    private DataInputStream inFromServer;


    private static ServerConnector instance = new ServerConnector();

/*    public static void main(String args[]){
        ServerConnector s = ServerConnector.getInstance();
        HashMap<String,String> m = new HashMap<>();
        m.put("username","z");
        m.put("password","z");
        s.sendRequest("UL2", m);
        ServerAnswer ans = s.getAnswer();
        System.out.println(ans.getType());
        System.out.println(ans.getStatus());
        System.out.println(ans.getContent());
    }*/

    public static ServerConnector getInstance(){
        return instance;
    }

    private ServerConnector(){
        initializeConnection();
    }

    private void initializeConnection(){
        try {
            //sock = new Socket("10.0.3.2", 5554);
            sock = new Socket("13.59.189.231", 5553);
            outToServer = new DataOutputStream(sock.getOutputStream());
            //inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream(), "UTF-8"));
            inFromServer = new DataInputStream(sock.getInputStream());
        } catch (IOException e) {
            outToServer = null;
            inFromServer = null;
        }
    }
    private void send (String request) throws Exception{
        if (outToServer == null || inFromServer == null){
            initializeConnection();
        }
        try {
            byte[] b = request.getBytes(Charset.forName("UTF-8"));
            outToServer.write(b);
        } catch (Exception e){
            outToServer = null;
            inFromServer = null;
            throw e;
        }
    }

    private String receive() throws Exception{
        if (outToServer == null || inFromServer == null){
            initializeConnection();
        }
        try {
            byte[] data = new byte[8192*2];
            int recv = 0;
            String answer = "";
            recv  = inFromServer.read(data);
            String num = new String(data,"UTF-8").split("\\{")[0];
            int size = Integer.parseInt(num);
            data = Arrays.copyOfRange(data, num.length(), recv);
            System.out.println(data.toString());
            recv = recv - num.length();
            int count = recv;

            while (count < size){
                byte[] temp = Arrays.copyOfRange(data, 0, recv);
                String tempStr = new String(temp, "UTF-8");
                answer += tempStr;
                recv  = inFromServer.read(data);
                count += recv;
            }
            byte[] temp = Arrays.copyOfRange(data, 0, recv);
            String tempStr = new String(temp, "UTF-8");
            answer += tempStr;

            //System.out.println("len after: " + );
            //System.out.println("recv: " + String.valueOf(recv));
            System.out.println(answer);
            //System.out.println(ans);
            return answer;
        } catch (Exception e){
            outToServer = null;
            inFromServer = null;
            throw e;
        }

    }
    private void closeConnection(){
        try {
            sock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendRequest(String command, HashMap<String, String> parms)throws Exception{
        JSONObject obj = new JSONObject();
        obj.put("command",command);
        JSONObject obj2 = new JSONObject();
        for(Map.Entry<String,String> entry : parms.entrySet()){
            obj2.put(entry.getKey(),entry.getValue());
        }
        obj.put("parms",obj2);
        System.out.println(obj.toJSONString());
        send(obj.toJSONString());
    }

    public ServerAnswer getAnswer() throws Exception{
        String ans =  receive();
        return new ServerAnswer(ans);

    }



}
