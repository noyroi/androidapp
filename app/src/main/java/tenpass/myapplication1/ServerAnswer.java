package tenpass.myapplication1;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by Noy on 15/04/2017.
 */
public class ServerAnswer {
    private JSONObject json;

    public ServerAnswer(String ans){
        JSONParser parser = new JSONParser();
        try {
            json = (JSONObject) parser.parse(ans);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getType(){
        return (String)json.get("type");
    }
    public String getContent(){
        JSONObject ans = (JSONObject)json.get("answer");
        return (String)ans.get("content");
    }
    public String getStatus(){
        JSONObject ans = (JSONObject)json.get("answer");
        return (String)ans.get("status");
    }

}
