package tenpass.myapplication1;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Calendar;
import java.util.HashMap;

public class SignupActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private EditText mDateDisplay;
    private int mYear;
    private int mMonth;
    private int mDay;
    private ConnectSocket mAuthTask;
    private ServerConnector s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mDateDisplay = (EditText)findViewById(R.id.dateDisplay);
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        ((EditText)findViewById(R.id.username)).setTypeface(font);
        ((EditText)findViewById(R.id.password)).setTypeface(font);
        ((EditText)findViewById(R.id.email)).setTypeface(font);
        ((EditText)findViewById(R.id.firstName)).setTypeface(font);
        ((EditText)findViewById(R.id.lastName)).setTypeface(font);
        ((EditText)findViewById(R.id.city)).setTypeface(font);
        ((EditText)findViewById(R.id.dateDisplay)).setTypeface(font);
        mDateDisplay.setTypeface(font);
    }

    public void changeDate(View view){
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "DatePicker");
    }


    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mYear = year;
        mMonth = monthOfYear + 1;
        mDay = dayOfMonth;
        String dd = String.valueOf(mDay);
        String mm = String.valueOf(mMonth);
        if (mDay < 10){
            dd = "0" + dd;
        }
        if (mMonth < 10){
            mm = "0" + mm;
        }
        mDateDisplay.setText(new StringBuilder()
                .append(dd).append("/").append(mm).append("/")
                .append(mYear).append(" "));
    }


    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        // Callback to DatePickerActivity.onDateSet() to update the UI
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            ((DatePickerDialog.OnDateSetListener) getActivity()).onDateSet(view, year, monthOfYear, dayOfMonth);
        }
    }

    public void submitSignup(View view){
        String username = ((EditText)findViewById(R.id.username)).getText().toString();
        String password = ((EditText)findViewById(R.id.password)).getText().toString();
        String email = ((EditText)findViewById(R.id.email)).getText().toString();
        String firstName = ((EditText)findViewById(R.id.firstName)).getText().toString();
        String lastName = ((EditText)findViewById(R.id.lastName)).getText().toString();
        String city = ((EditText)findViewById(R.id.city)).getText().toString();
        String birthday = ((EditText)findViewById(R.id.dateDisplay)).getText().toString();
        if(!username.isEmpty() && !password.isEmpty() && !email.isEmpty() && !firstName.isEmpty()
                && ! lastName.isEmpty() && !city.isEmpty() && !birthday.isEmpty()) {
            mAuthTask = new ConnectSocket(username,  password,  email,  firstName,
                     lastName, city, birthday);
            mAuthTask.execute();
        }
        else
        {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SignupActivity.this);
            dlgAlert.setMessage("חובה למלא את כל השדות!");
            dlgAlert.setTitle("אופס...");
            dlgAlert.setPositiveButton("הבנתי", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
            dlgAlert.setPositiveButton("הבנתי", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
    }

    public class ConnectSocket extends AsyncTask<Void, Void, String> {
        private String username;
        private String password;
        private String email;
        private String firstName;
        private String lastName;
        private String city;
        private String birthday;

        public ConnectSocket(String username, String password, String email, String firstName,
                             String lastName,String city,String birthday){
            this.username = username;
            this.password = password;
            this.email = email;
            this.firstName = firstName;
            this.lastName = lastName;
            this.city = city;
            this.birthday = birthday;
        }


        @Override
        protected String doInBackground(Void... params) {
            s = ServerConnector.getInstance();
            HashMap<String, String> m = new HashMap<>();
            m.put("username", username);
            m.put("password", password);
            m.put("email", email);
            m.put("firstName", firstName);
            m.put("lastName", lastName);
            m.put("city", city);
            m.put("birthday", birthday);

            try {
                s.sendRequest("UL1", m);
                ServerAnswer a = s.getAnswer();
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                if (a.getType().equals("UL1") && a.getStatus().equals("success")) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("username", username);
                    editor.putString("password", password);
                    editor.putString("uid", a.getContent());
                    editor.commit();
                    Intent i = new Intent(SignupActivity.this, MenuActivity.class);
                    startActivity(i);
                    setResult(RESULT_OK,null);
                    finish();

                } else if (a.getStatus() != "success") {
                    return a.getContent();
                }
            } catch (Exception e) {
                return "serverError";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.equals("")) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SignupActivity.this);
                if(answer.contains("email")) {
                    dlgAlert.setMessage("אימייל זה כבר תפוס");
                }
                if(answer.contains("username")) {
                    dlgAlert.setMessage("שם משתמש זה כבר בשימוש");
                }
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            } else if (answer.equals("serverError")){
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(SignupActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
            }
        }
    }

}
