package tenpass.myapplication1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;

public class MenuActivity extends AppCompatActivity implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private String gameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    public void findGame(View view){
        Intent i = new Intent(MenuActivity.this, SearchActivity.class);
        startActivity(i);
    }
    public void createGame(View view){
        Intent i = new Intent(MenuActivity.this, MapsActivity.class);
        startActivity(i);
    }
    public void getRandomGame(View view){
        //Algorithm for finding the best game for this player
        //Async thats gets an id of a game
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        MenuActivity.GetGame mAuthTask = new MenuActivity.GetGame(settings.getString("uid","0"),
                mLastLocation.getLatitude(), mLastLocation.getLongitude());
        mAuthTask.execute();
    }
    public void goToProfile(View view){
        Intent i = new Intent(MenuActivity.this, ProfileActivity.class);
        startActivity(i);
    }
    public void goToSettings(View view){
        Intent i = new Intent(MenuActivity.this, SettingsActivity.class);
        startActivity(i);

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }



    public class GetGame extends AsyncTask<Void, Void, String> {
        private String userId;
        private double LA,LO;

        public GetGame(String u, double la, double lo) {
            this.userId = u;
            this.LA = la;
            this.LO = lo;
        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();
            m.put("userId", userId);
            m.put("LA", String.valueOf(LA));
            m.put("LO", String.valueOf(LO));
            try {
                s.sendRequest("FL1", m);
                ServerAnswer a = s.getAnswer();

                if (a.getType().equals("FL1") && a.getStatus().equals("success")) {
                    return a.getContent().toString();
                }
            } catch (Exception e) {
                return "";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()) {
                Intent i = new Intent(MenuActivity.this, GameActivity.class);
                i.putExtra("id", Integer.parseInt(answer));
                i.putExtra("fromProfile", false);
                startActivity(i);
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MenuActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                dlgAlert.create().show();
            }
        }
    }
}
