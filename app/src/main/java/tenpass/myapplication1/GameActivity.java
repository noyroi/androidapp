package tenpass.myapplication1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.HashMap;

public class GameActivity extends AppCompatActivity {

    private long maxNum = -1;
    private int numOfPlayers = -1;
    private String answer = "";
    private String uid;
    private boolean flag = false;
    private GameActivity mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        this.uid = String.valueOf((int)getIntent().getExtras().get("id"));
        if ((boolean)getIntent().getExtras().get("fromProfile")){
            findViewById(R.id.join).setVisibility(View.INVISIBLE);
        }

        this.mContext = this;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                ServerConnector s = ServerConnector.getInstance();
                HashMap<String, String> m = new HashMap<>();
                m.put("uid", uid);
                try {
                    s.sendRequest("GG1", m);
                    ServerAnswer a = s.getAnswer();
                    if (a.getType().equals("GG1") && a.getStatus().equals("success")) {
                        answer = a.getContent();
                        flag = true;
                        JSONObject answers;
                        JSONArray players;
                        JSONParser parser = new JSONParser();
                        try {
                            answers = (org.json.simple.JSONObject) parser.parse(answer);
                            players = (org.json.simple.JSONArray)  answers.get("players");
                            //TODO add loader
                            TextView txt = (TextView) findViewById(R.id.header);
                            TextView playerstitle = (TextView) findViewById(R.id.playerstitle);
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            txt.setTypeface(font);
                            playerstitle.setTypeface(font);
                            String date = ((String)answers.get("date")).substring(0,10);
                            String time = ((String)answers.get("time"));
                            txt.setText("משחק ב"+ (String) answers.get("courtName")+ " " + "\n" + "ביום "+ date + " בשעה "+ time );

                            TextView txtdetails = (TextView) findViewById(R.id.details);
                            txtdetails.setTypeface(font);
                            String priv;
                            maxNum = (long)answers.get("numOfPlayers");
                            numOfPlayers = players.size();
                            if ((boolean) answers.get("private")){
                                priv = "המשחק סגור לחברים בלבד וייארך כ";
                            } else {
                                priv = "המשחק פתוח לכולם וייארך כ";
                            }
                            txtdetails.setText(priv + String.valueOf((long)answers.get("duration")) + " דקות");

                            GridLayout gridLayout = (GridLayout)findViewById(R.id.players);
                            gridLayout.setColumnCount(3);
                            int mod = players.size() % 3;
                            int size = players.size() /3;
                            if (mod != 0)
                                size++;

                            gridLayout.setColumnCount(3);
                            gridLayout.setRowCount(size);
                            int i=0, c=0, r=0;
                            for (Object obj: players) {
                                if(c == 3)
                                {
                                    c = 0;
                                    r++;
                                }
                                GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams();
                                gridParam.height = GridLayout.LayoutParams.WRAP_CONTENT;
                                gridParam.width = GridLayout.LayoutParams.WRAP_CONTENT;
                                gridParam.rightMargin = 100;
                                gridParam.topMargin = 70;
                                gridParam.setGravity(Gravity.CENTER);
                                gridParam.columnSpec = GridLayout.spec(c);
                                gridParam.rowSpec = GridLayout.spec(r);
                                JSONObject playerObject = (JSONObject) obj;
                                LinearLayout ll = new LinearLayout(mContext);
                                TextView user = new TextView(mContext);
                                //lparams.gravity = Gravity.RIGHT;
                                //lparams.setMargins(10, 10, 10, 10);
                                ll.setOrientation(LinearLayout.VERTICAL);
                                ll.setLayoutParams(gridParam);
                                String cont = (String)playerObject.get("username") + ", " +  String.valueOf((long)playerObject.get("age"));
                                user.setText(cont);
                                user.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                user.setTextColor(Color.parseColor("#000000"));

                                ll.addView(user);
                                ImageView img = new ImageView(mContext);
                                switch (i%4){
                                    case 0:
                                        img.setImageResource(R.drawable.player1);
                                        break;
                                    case 1:
                                        img.setImageResource(R.drawable.player2);
                                        break;
                                    case 2:
                                        img.setImageResource(R.drawable.player3);
                                        break;
                                    case 3:
                                        img.setImageResource(R.drawable.player4);
                                        break;
                                }
                                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(164,194);
                                img.setLayoutParams(params);
                                ll.addView(img);
                                gridParam.width = 400;
                                gridLayout.addView(ll,i,gridParam);
                                i++; c++;
                            }

                        } catch (Exception e) {
                            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                            dlgAlert.setMessage("שגיאת התחברות לשרת");
                            dlgAlert.setTitle("אופס...");
                            dlgAlert.setCancelable(true);
                            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent i = new Intent(GameActivity.this, MenuActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });
                            dlgAlert.create().show();
                        }
                    }

                } catch (Exception e) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                    dlgAlert.setMessage(e.getMessage());
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setCancelable(true);
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(GameActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    dlgAlert.create().show();
                }
            }
        });
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
            dlgAlert.setMessage(e.getMessage());
            dlgAlert.setTitle("אופס...");
            dlgAlert.setCancelable(true);
            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(GameActivity.this, MenuActivity.class);
                    startActivity(i);
                    finish();
                }
            });
            dlgAlert.create().show();
        }


        //GameActivity.ConnectSocket mAuthTask =
        //        new GameActivity.ConnectSocket(String.valueOf((int)getIntent().getExtras().get("id")),this);
        //mAuthTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void joinGame(View view){
        if(maxNum == -1 || numOfPlayers == -1 ){
            return;
        }
        if(numOfPlayers < maxNum) {
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            GameActivity.JoinGameCommand mAuthTask = new GameActivity.JoinGameCommand
                    (settings.getString("uid", "0"), String.valueOf((int) getIntent().getExtras().get("id")), this);
            mAuthTask.execute();
        }
        else{
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
            String txt = "משחק זה מוגבל ל" + maxNum + " שחקנים";
            dlgAlert.setMessage(txt);
            dlgAlert.setTitle("אוי פיספסת...");
            dlgAlert.setCancelable(true);
            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dlgAlert.create().show();
        }
    }



    private class JoinGameCommand extends AsyncTask<Void, Void, String> {
        private String userId;
        private String gameId;
        private Context mContext;

        public JoinGameCommand(String user, String game, GameActivity gameActivity) {
            super();
            this.userId = user;
            this.gameId = game;
            this.mContext = gameActivity;
        }

        @Override
        protected String doInBackground(Void... params) {
            String answer = "";
            ServerConnector s = ServerConnector.getInstance();
            HashMap<String, String> m = new HashMap<>();
            m.put("userId", userId);
            m.put("gameId", gameId);
            try {
                s.sendRequest("AU1", m);
                ServerAnswer a = s.getAnswer();

                if (a.getType().equals("AU1") && a.getStatus().equals("success")) {
                    return "success";
                }

            } catch (Exception e) {
                return "fail";
            }
            return "fail";
        }


        protected void onPostExecute(final String answer) {
            if (!answer.equals("success")) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת. אנא נסה שנית מאוחר יותר");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(GameActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                dlgAlert.create().show();
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                dlgAlert.setMessage("הצטרפת למשחק בהצלחה!");
                dlgAlert.setTitle("יופי");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("תודה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(getIntent());
                    }
                });
                dlgAlert.create().show();
            }
        }
    }


    private class ConnectSocket extends AsyncTask<Void, Void, Void> {

        private String uid;
        private Context mContext;
        private boolean flag = true;

        public ConnectSocket(String uid, GameActivity gameActivity) {
            super();
            this.uid = uid;
            this.mContext = gameActivity;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            HashMap<String, String> m = new HashMap<>();
            m.put("uid", uid);
            try {
                s.sendRequest("GG1", m);
                ServerAnswer a = s.getAnswer();

                if (a.getType().equals("GG1") && a.getStatus().equals("success")) {
                    answer = a.getContent();
                    flag = true;
                }

            } catch (Exception e) {
                flag = false;
            }
            return null;
        }


        protected void onPostExecute(final Void x) {
            if (flag){
                JSONObject answers;
                JSONArray players;
                JSONParser parser = new JSONParser();
                try {
                    answers = (org.json.simple.JSONObject) parser.parse(answer);
                    players = (org.json.simple.JSONArray)  answers.get("players");
                    //TODO add loader
                    TextView txt = (TextView) findViewById(R.id.header);
                    TextView playerstitle = (TextView) findViewById(R.id.playerstitle);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    txt.setTypeface(font);
                    playerstitle.setTypeface(font);
                    String date = ((String)answers.get("date")).substring(0,10);
                    String time = ((String)answers.get("time"));
                    txt.setText("משחק ב"+ (String) answers.get("courtName")+ " " + "\n" + "ביום "+ date + " בשעה "+ time );

                    TextView txtdetails = (TextView) findViewById(R.id.details);
                    txtdetails.setTypeface(font);
                    String priv;
                    maxNum = (long)answers.get("numOfPlayers");
                    numOfPlayers = players.size();
                    if ((boolean) answers.get("private")){
                        priv = "המשחק סגור לחברים בלבד וייארך כ";
                    } else {
                        priv = "המשחק פתוח לכולם וייארך כ";
                    }
                    txtdetails.setText(priv + String.valueOf((long)answers.get("duration")) + " דקות");

                    GridLayout gridLayout = (GridLayout)findViewById(R.id.players);
                    gridLayout.setColumnCount(3);
                    int mod = players.size() % 3;
                    int size = players.size() /3;
                    if (mod != 0)
                        size++;

                    gridLayout.setColumnCount(3);
                    gridLayout.setRowCount(size);
                    int i=0, c=0, r=0;
                    for (Object obj: players) {

                        if(c == 3)
                        {
                            c = 0;
                            r++;
                        }
                        GridLayout.LayoutParams gridParam = new GridLayout.LayoutParams();
                        gridParam.height = GridLayout.LayoutParams.WRAP_CONTENT;
                        gridParam.width = GridLayout.LayoutParams.WRAP_CONTENT;
                        gridParam.rightMargin = 100;
                        gridParam.topMargin = 70;
                        gridParam.setGravity(Gravity.CENTER);
                        gridParam.columnSpec = GridLayout.spec(c);
                        gridParam.rowSpec = GridLayout.spec(r);
                        JSONObject playerObject = (JSONObject) obj;
                        long uid = (long)playerObject.get("uid");
                        LinearLayout ll = new LinearLayout(this.mContext);
                        TextView user = new TextView(this.mContext);
                        //lparams.gravity = Gravity.RIGHT;
                        //lparams.setMargins(10, 10, 10, 10);
                        ll.setOrientation(LinearLayout.VERTICAL);
                        ll.setLayoutParams(gridParam);
                        String cont = (String)playerObject.get("username") + ", " +  String.valueOf((long)playerObject.get("age"));
                        user.setText(cont);
                        user.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                        user.setTextColor(Color.parseColor("#000000"));

                        ll.addView(user);
                        ImageView img = new ImageView(this.mContext);
                        switch (i%4){
                            case 0:
                                img.setImageResource(R.drawable.player1);
                                break;
                            case 1:
                                img.setImageResource(R.drawable.player2);
                                break;
                            case 2:
                                img.setImageResource(R.drawable.player3);
                                break;
                            case 3:
                                img.setImageResource(R.drawable.player4);
                                break;
                        }
                        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(164,194);
                        img.setLayoutParams(params);
                        ll.addView(img);
                        gridParam.width = 400;
                        gridLayout.addView(ll,i,gridParam);
                        i++; c++;
                    }

                } catch (Exception e) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                    dlgAlert.setMessage("שגיאת התחברות לשרת");
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setCancelable(true);
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(GameActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    dlgAlert.create().show();
                }
            }
            else{
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(GameActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(GameActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                dlgAlert.create().show();

            }
        }
    }
}
