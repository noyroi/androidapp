package tenpass.myapplication1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        TextView t = (TextView)findViewById(R.id.gamestitle) ;
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        t.setTypeface(font);
        getPlayersDetails();


    }


    public void getPlayersDetails(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        ConnectSocket mAuthTask = new ConnectSocket(settings.getString("uid","0"),this);
        mAuthTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }


    public class ConnectSocket extends AsyncTask<Void, Void, String> {
        private String uid1;
        private Context mContext;

        public ConnectSocket(String u, Context context){
            this.uid1 = u;
            this.mContext = context;

        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String,String> m = new HashMap<>();
            m.put("uid", uid1);
            try {
                s.sendRequest("UP3", m);
                ServerAnswer a = s.getAnswer();
                s.sendRequest("UG1", m);
                ServerAnswer a2 = s.getAnswer();

                if (a.getType().equals("UP3") && a.getStatus().equals("success")){
                    if (a2.getType().equals("UG1") && a2.getStatus().equals("success")){
                        JSONObject answers = new JSONObject();
                        answers.put("details", a.getContent());
                        answers.put("games", a2.getContent());
                        return answers.toString();
                    }
                }
            } catch (Exception e) {
                return "";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()){
                JSONObject answers;
                JSONObject answer1;
                JSONArray answer2;
                JSONParser parser = new JSONParser();
                try {
                    answers = (org.json.simple.JSONObject) parser.parse(answer);
                    answer1 = (org.json.simple.JSONObject) parser.parse((String) answers.get("details"));
                    answer2 = (org.json.simple.JSONArray) parser.parse((String) answers.get("games"));
                    //TODO add loader
                    TextView username = (TextView) findViewById(R.id.username);
                    TextView name = (TextView) findViewById(R.id.name);
                    TextView city = (TextView) findViewById(R.id.city);
                    TextView birthday = (TextView) findViewById(R.id.birthdate);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    username.setTypeface(font);
                    name.setTypeface(font);
                    city.setTypeface(font);
                    birthday.setTypeface(font);
                    username.setText((String)answer1.get("username"));
                    name.setText((String)answer1.get("firstName") + " " +(String)answer1.get("lastName"));
                    city.setText((String)answer1.get("city"));
                    String b = (String)answer1.get("birthday");
                    b = b.substring(0,10);
                    long age = (long)answer1.get("age");
                    birthday.setText(b + " (" + String.valueOf(age) +")");

                    LinearLayout linearLayout = (LinearLayout)findViewById(R.id.games);
                    LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                    int i = 0;
                    Calendar c = Calendar.getInstance();
                    Date today = c.getTime();
                   for (Object obj: answer2) {
                       JSONObject gameObject = (JSONObject) obj;
                       SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                       c.setTime(df.parse((String) (gameObject.get("date"))));
                       if (today.after(c.getTime())){
                           continue;
                       }
                       LinearLayout lh = new LinearLayout(this.mContext);
                       lh.setOrientation(LinearLayout.HORIZONTAL);
                       lh.setGravity(Gravity.RIGHT);
                       ImageView img = new ImageView(this.mContext);
                       switch (i%9){
                           case 0:
                               img.setImageResource(R.drawable.icon1);
                               break;
                           case 1:
                               img.setImageResource(R.drawable.icon2);
                               break;
                           case 2:
                               img.setImageResource(R.drawable.icon3);
                               break;
                           case 3:
                               img.setImageResource(R.drawable.icon4);
                               break;
                           case 4:
                               img.setImageResource(R.drawable.icon5);
                               break;
                           case 5:
                               img.setImageResource(R.drawable.icon6);
                               break;
                           case 6:
                               img.setImageResource(R.drawable.icon7);
                               break;
                           case 7:
                               img.setImageResource(R.drawable.icon8);
                               break;
                           case 8:
                               img.setImageResource(R.drawable.icon9);
                               break;
                       }


                        TextView txt = new TextView(this.mContext);
                        lparams.gravity = Gravity.RIGHT;
                        lparams.setMargins(10, 20, 10, 10);
                        txt.setLayoutParams(lparams);
                        txt.setTypeface(font);
                       String cont = "תאריך:" + " " + (String) (gameObject.get("date")) + "\n" +
                                     "שעה:" + " " + (String) (gameObject.get("time")) + "\n" +
                                     "משך:" + " " + String.valueOf((long)(gameObject.get("duration")))+" " +"דקות"+ "\n" +
                                     "מארגן:" + " " + (String) (gameObject.get("creator"));
                        txt.setText(cont);
                        txt.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                        txt.setTextColor(Color.parseColor("#000000"));
                       txt.setId((int)((long)gameObject.get("id")));
                       txt.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                               int uid = ((TextView)v).getId();
                               Intent i = new Intent(ProfileActivity.this, GameActivity.class);
                               i.putExtra("id", uid);
                               i.putExtra("fromProfile", true);
                               startActivity(i);
                           }
                       });
                        //linearLayout.addView(txt);
                       lh.addView(txt);
                       LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(130, 130);
                       lp.setMargins(10, 100, 10, 10);
                       img.setLayoutParams(lp);
                       lh.addView(img);
                       linearLayout.addView(lh);

                        View line = new View(this.mContext);
                       line.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 1));
                       line.setBackgroundColor(Color.GRAY);
                       linearLayout.addView(line);

                       i++;
                   }

                } catch (Exception e) {
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ProfileActivity.this);
                    dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setCancelable(true);
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(ProfileActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                    dlgAlert.create().show();

                }
            }
            else{
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(ProfileActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(ProfileActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                dlgAlert.create().show();

            }
        }
    }

}
