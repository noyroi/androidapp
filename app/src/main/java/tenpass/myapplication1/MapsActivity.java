package tenpass.myapplication1;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private Map<Marker, String> allMarkers = new HashMap<Marker, String>();
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private HashMap<String, HashMap<Integer, String>> cityAndCourt;
    private ArrayList<String> CityList = new ArrayList<String>();
    private ArrayList<String> CourtList = new ArrayList<String>();
    private boolean FillCourt = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            public View getInfoWindow(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);
                ((TextView) v.findViewById(R.id.markertitle)).setText(marker.getTitle());
                ((TextView) v.findViewById(R.id.markertext)).setText(marker.getSnippet());
                return v;
            }

            public View getInfoContents(Marker marker) {

                //View v = getLayoutInflater().inflate(R.layout.custom_infowindow, null);

                return null;

            }
        });
        mMap.setOnMarkerClickListener(this);
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        MapsActivity.ConnectSocket mAuthTask = new MapsActivity.ConnectSocket(this);
        mAuthTask.execute();
        // Add a marker in tel aviv
        //LatLng tel = new LatLng(32.059954, 34.805677);
        //mMap.addMarker(new MarkerOptions().position(tel).title("פארק אדית וולפסון"));
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {

            // mMap.moveCamera(CameraUpdateFactory.newLatLng(tel));
            float zoomLevel = (float) 16.0; //This goes up to 21
            LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mMap.setMyLocationEnabled(true);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, zoomLevel));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        String city = allMarkers.get(marker);
        String court = marker.getTitle();
        Spinner citySpinner = (Spinner) findViewById(R.id.cities);
        Spinner courtSpinner = (Spinner) findViewById(R.id.courts);
        if (citySpinner.getSelectedItem().toString().equals(city)) {
            if (courtSpinner.getSelectedItem().toString().equals(court)) {

            } else {

                selectSpinnerItemByValue(courtSpinner, court);
            }
        } else {
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) parent.getChildAt(0)).setTypeface(font);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            selectSpinnerItemByValue(citySpinner, city);
            FillCourt = true;
            fillCourtSpinner(citySpinner);
            FillCourt = false;
            selectSpinnerItemByValue(courtSpinner, court);
            citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                    ((TextView) parent.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                    Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                    ((TextView) parent.getChildAt(0)).setTypeface(font);
                    fillCourtSpinner(parent);
                    FillCourt = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        return true;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }
    public void courtChosen(View view) {
        Spinner citySpinner = (Spinner) findViewById(R.id.cities);
        Spinner courtSpinner = (Spinner) findViewById(R.id.courts);
        String city = citySpinner.getSelectedItem().toString();
        String name = courtSpinner.getSelectedItem().toString();
        int uid = 0;
        if (!city.equals("") && !name.equals("")) {
            for (HashMap.Entry<Integer, String> entry : cityAndCourt.get(city).entrySet()) {
                if (entry.getValue().equals(name)) {
                    uid = entry.getKey();
                    break;
                }
            }
            Intent i = new Intent(MapsActivity.this, NewGameActivity.class);
            i.putExtra("courtId", uid);
            i.putExtra("courtCity", city);
            i.putExtra("courtName", name);
            startActivityForResult(i, 0);
            //startActivity(i);
        } else {
            android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(MapsActivity.this);
            dlgAlert.setMessage("יש לבחור מגרש כדי להמשיך...");
            dlgAlert.setTitle("אופס...");
            dlgAlert.setPositiveButton("סבבה", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent i = new Intent(MapsActivity.this, MenuActivity.class);
                    startActivity(i);
                    finish();
                }
            });
        }
    }

    public static void selectSpinnerItemByValue(Spinner spnr, String value) {
        ArrayAdapter adapter = (ArrayAdapter) spnr.getAdapter();

        int p = adapter.getPosition(value);
        spnr.setSelection(p);
    }

    private void fillCourtSpinner(AdapterView<?> parentView) {
        if (FillCourt) {
            HashMap<Integer, String> courts = cityAndCourt.get(parentView.getSelectedItem().toString());
            if (courts != null) {
                CourtList.clear();
                for (String value : courts.values()) {
                    CourtList.add(value);
                }
                CourtList.add("בחר מגרש");
                ArrayAdapter<String> arrayAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, CourtList) {
                    @Override
                    public View getDropDownView(int position, View convertView, ViewGroup parent) {
                        View v = super.getDropDownView(position, convertView, parent);
                        ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                        ((TextView) v).setTypeface(font);
                        return v;
                    }

                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {

                        View v = super.getView(position, convertView, parent);
                        if (position == getCount()) {
                            ((TextView) v.findViewById(android.R.id.text1)).setText("");
                            ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                            ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);

                        }
                        return v;
                    }

                    @Override
                    public int getCount() {
                        return super.getCount() - 1;
                    }

                };
                arrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                Spinner courtSpinner = (Spinner) findViewById(R.id.courts);
                courtSpinner.setAdapter(arrayAdapter2);
                Drawable spinnerDrawable = courtSpinner.getBackground().getConstantState().newDrawable();
                spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    courtSpinner.setBackground(spinnerDrawable);
                } else {
                    courtSpinner.setBackgroundDrawable(spinnerDrawable);
                }
                courtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                        ((TextView) parent.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                        ((TextView) parent.getChildAt(0)).setTypeface(font);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                courtSpinner.setSelection(arrayAdapter2.getCount());
            }
        }
    }


    public class ConnectSocket extends AsyncTask<Void, Void, String> {
        private Context mContext;

        public ConnectSocket(MapsActivity mapsActivity) {
            this.mContext = mapsActivity;
        }

        @Override
        protected String doInBackground(Void... params) {
            ServerConnector s = ServerConnector.getInstance();
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = settings.edit();
            HashMap<String, String> m = new HashMap<>();

            try {
                s.sendRequest("CA1", m);
                ServerAnswer a = s.getAnswer();
                if (a.getType().equals("CA1") && a.getStatus().equals("success")) {
                    return a.getContent().toString();
                }
            } catch (Exception e) {
                return "";
            }
            return "";
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()) {
                cityAndCourt = new HashMap<String, HashMap<Integer, String>>();
                JSONArray answers;
                JSONParser parser = new JSONParser();
                try {
                    answers = (org.json.simple.JSONArray) parser.parse(answer);
                    for (Object obj : answers) {
                        JSONObject courtObject = (JSONObject) obj;
                        String des = (String) courtObject.get("street") + " " + String.valueOf((long) courtObject.get("streetNum")) + ", " +
                                (String) courtObject.get("city") + "\n";
                        switch ((String) courtObject.get("kind")) {
                            case "A":
                                des += "אספלט" + "\n";
                                break;
                            case "S":
                                des += "חול" + "\n";
                                break;
                            default:
                                des += "דשא" + "\n";
                        }
                        if ((long) courtObject.get("width") * (long) courtObject.get("length") + 50 > 90 * 50) {
                            des += "גדול מהרגיל" + "\n";
                        } else if ((long) courtObject.get("width") * (long) courtObject.get("length") - 50 < 90 * 50) {
                            des += "קטן מהרגיל" + "\n";
                        } else {
                            des += "גודל סטנדרטי" + "\n";
                        }
                        des += (String) courtObject.get("description");
                        LatLng tel = new LatLng((double) courtObject.get("LA"), (double) courtObject.get("LO"));
                        String name = (String) courtObject.get("name");
                        MarkerOptions mp = new MarkerOptions().position(tel).snippet(des).title(name);
                        Marker m = mMap.addMarker(mp);
                        String city = (String) courtObject.get("city");
                        allMarkers.put(m, city);

                        if (!cityAndCourt.containsKey(city)) {
                            HashMap<Integer, String> cityDic = new HashMap<Integer, String>();
                            cityDic.put((int) ((long) courtObject.get("id")), name);
                            cityAndCourt.put(city, cityDic);
                            CityList.add(city);
                        } else {
                            cityAndCourt.get(city).put((int) ((long) courtObject.get("id")), name);
                        }
                    }
                    CityList.add("בחר עיר");
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this.mContext, android.R.layout.simple_spinner_dropdown_item, CityList) {
                        @Override
                        public View getDropDownView(int position, View convertView, ViewGroup parent) {
                            View v = super.getDropDownView(position, convertView, parent);
                            ((TextView) v).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            ((TextView) v).setTypeface(font);
                            return v;
                        }

                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {

                            View v = super.getView(position, convertView, parent);
                            if (position == getCount()) {
                                ((TextView) v.findViewById(android.R.id.text1)).setText("");
                                ((TextView) v.findViewById(android.R.id.text1)).setHintTextColor(Color.GRAY);
                                ((TextView) v.findViewById(android.R.id.text1)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                ((TextView) v.findViewById(android.R.id.text1)).setHint(getItem(getCount()));
                                Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                                ((TextView) v.findViewById(android.R.id.text1)).setTypeface(font);
                            }
                            return v;
                        }


                        @Override
                        public int getCount() {
                            return super.getCount() - 1;
                        }

                    };
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    Spinner citySpinner = (Spinner) findViewById(R.id.cities);
                    citySpinner.setAdapter(arrayAdapter);
                    Drawable spinnerDrawable = citySpinner.getBackground().getConstantState().newDrawable();
                    spinnerDrawable.setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        citySpinner.setBackground(spinnerDrawable);
                    } else {
                        citySpinner.setBackgroundDrawable(spinnerDrawable);
                    }

                    citySpinner.setSelection(arrayAdapter.getCount());

                    citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            ((TextView) parentView.getChildAt(0)).setTextColor(Color.BLACK);
                            ((TextView) parentView.getChildAt(0)).setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                            Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
                            ((TextView) parentView.getChildAt(0)).setTypeface(font);
                            fillCourtSpinner(parentView);
                            FillCourt = true;
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });


                } catch (Exception e) {
                    android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(MapsActivity.this);
                    dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                    dlgAlert.setTitle("אופס...");
                    dlgAlert.setPositiveButton("סבבה", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();
                    dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(MapsActivity.this, MenuActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });
                }
            } else {
                android.app.AlertDialog.Builder dlgAlert = new android.app.AlertDialog.Builder(MapsActivity.this);
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.create().show();
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MapsActivity.this, MenuActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
            }
        }
    }
}


