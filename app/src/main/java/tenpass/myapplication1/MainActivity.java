package tenpass.myapplication1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.CountDownTimer;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private UserLoginMainTask mAuthTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkForLoginDetails();
    }

    private void checkForLoginDetails() {
        Thread t = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                Intent intent;
                //in case we want to add first time guide
                /*

                if(settings.getString("firstTime", "Yes").equals("Yes")){
                    editor.putString("firstTime", "No");
                    editor.commit();
                    intent = new Intent(MainActivity.this, ExpActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                } else*/
                //editor.clear();
               //editor.commit();
                //editor.putString("username", "User not found");
                if (settings.getString("username", "User not found").equals("User not found")) {
                    intent = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                }
                else{
                    //editor.putString("username", "z");
                    //editor.putString("password", "z");
                    //editor.commit();
                    mAuthTask = new UserLoginMainTask(settings.getString("username",""),
                            settings.getString("password", ""));
                    mAuthTask.execute();
                }
            }
        });
        t.start();
    }


    public class UserLoginMainTask extends AsyncTask<Void, Void, String> {
        private String user;
        private String pass;

        public UserLoginMainTask(String u,String p){
            this.user = u;
            this.pass = p;
        }
        @Override
        protected String doInBackground(Void... params) {
            String ans = null;
            try {
                ServerConnector s = ServerConnector.getInstance();
                HashMap<String,String> m = new HashMap<>();
                m.put("username",user);
                m.put("password",pass);
                s.sendRequest("UL2", m);
                ServerAnswer a = s.getAnswer();
                if (a.getType().equals("UL2") && a.getStatus().equals("success")){
                    ans = a.getContent();
                }
                else if (a.getStatus() != "success"){
                    Intent i = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
                //validate user+ password insert socket answer to ans
            } catch (Exception e) {
                return null;
            }

            return ans;
        }

        @Override
        protected void onPostExecute(final String answer) {
            if (answer != null) {
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("username", user);
                editor.putString("password", pass);
                editor.putString("uid", answer);
                editor.commit();
                Intent i = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(i);
                finish();
            } else {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(MainActivity.this);
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                });
                dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");
                dlgAlert.create().show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
}
