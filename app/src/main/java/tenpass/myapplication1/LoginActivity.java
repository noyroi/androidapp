package tenpass.myapplication1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {
    private ConnectSocket mAuthTask;
    private ServerConnector s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText e1 = (EditText) findViewById(R.id.username);
        EditText e2 = (EditText) findViewById(R.id.password);
        Typeface font = Typeface.createFromAsset(getAssets(), "GveretLevinAlefAlefAlef-Regular.otf");
        e1.setTypeface(font);
        e2.setTypeface(font);
    }

    public void signUp(View view){
        Intent i = new Intent(LoginActivity.this, SignupActivity.class);
        startActivityForResult(i, 0);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }





    public void submitLogin(View view){
        EditText e1 = (EditText) findViewById(R.id.username);
        EditText e2 = (EditText) findViewById(R.id.password);

        String user = e1.getText().toString();
        String pass = e2.getText().toString();
        if(!user.isEmpty() && !pass.isEmpty()) {
            mAuthTask = new ConnectSocket(user,pass);
            mAuthTask.execute();
        }
        else
        {

            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(LoginActivity.this);
            dlgAlert.setMessage("חובה למלא את כל השדות!");
            dlgAlert.setTitle("אופס...");
            dlgAlert.setPositiveButton("סבבה", null);
            dlgAlert.setCancelable(true);
            dlgAlert.create().show();
            dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
        }
    }

    public class ConnectSocket extends AsyncTask<Void, Void, String> {
        private String user;
        private String pass;

        public ConnectSocket(String u,String p){
            this.user = u;
            this.pass = p;
        }

        @Override
        protected String doInBackground(Void... params) {
            s = ServerConnector.getInstance();
            HashMap<String, String> m = new HashMap<>();
            m.put("username", user);
            m.put("password", pass);
            try {
                s.sendRequest("UL2", m);
                ServerAnswer a = s.getAnswer();
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                if (a.getType().equals("UL2") && a.getStatus().equals("success")) {
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("username", user);
                    editor.putString("password", pass);
                    editor.putString("uid", a.getContent());
                    editor.commit();
                    Intent i = new Intent(LoginActivity.this, MenuActivity.class);
                    startActivity(i);
                    finish();
                } else if (a.getStatus() != "success") {
                    return "fail";
                }
                return "";
            } catch (Exception e) {
                return "serverError";
            }
        }

        protected void onPostExecute(final String answer) {
            if (!answer.isEmpty()) {
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(LoginActivity.this);
                dlgAlert.setTitle("אופס...");
                dlgAlert.setPositiveButton("סבבה", null);
                dlgAlert.setCancelable(true);
                dlgAlert.setPositiveButton("סבבה", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                if (answer.equals("fail")) {
                    dlgAlert.setMessage("שם משתמש או סיסמא לא נכונים!");
                } else if (answer.equals("serverError")) {
                    dlgAlert.setMessage("שגיאת התחברות לשרת! נסה מאוחר יותר.");

                }
                dlgAlert.create().show();
            }
        }

    }
}
